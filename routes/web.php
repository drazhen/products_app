<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index');

Route::get('/product/create', 'ProductController@create');

Route::get('/product/index', 'ProductController@index');

Route::get('/product/edit/{id}', 'ProductController@edit');

Route::get('/product/delete/{id}', 'ProductController@delete');

Route::get('/product/destroy/{id}', 'ProductController@destroy');

Route::get('/product/{id}', 'ProductController@show');

Route::post('/product/store', 'ProductController@store');

Route::post('/product/update', 'ProductController@update');