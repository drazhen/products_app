<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB; 


class ProductController extends Controller
{

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:50',
            'price' => 'max:10000',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) 
        {
            return view('product.create');
        }
        else
        {
            return view('auth/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'price' => 'numeric|min:0|max:10000',
        ]);
        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        
        $product = new Product;

        $product->name = $name;
        $product->description = $description;
        $product->price = $price;
        $product->save();

        $products = Product::all();
        return view('product.index', compact('products'))->with('notice', 'New product saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$product = DB::table('products')->where('id', $id)->first();
        $product = Product::find($id);
        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) 
        {
            $product = Product::find($id);
            return view('product.edit', compact('product'));
        }
        else
        {
            return view('auth/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'price' => 'numeric|min:0|max:10000',
        ]);
        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $id = $request->input('id');

        $product = Product::find($id);

        // echo "id: " . $id;
        // echo $product->id;
        // echo $product->name;

        $product->name = $name;
        $product->description = $description;
        $product->price = $price;
        $product->save();

        $products = Product::all();
        return view('product.index', compact('products'))->with('notice', 'Product updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function delete(Product $product)
    {
        if (Auth::check()) 
        {
            return view('product.delete');
        }
        else
        {
            return view('auth/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        $products = Product::all();
        return view('product.index', compact('products'))->with('notice', 'Product deleted');
    }
}
