<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	public $timestamps = false;
    protected $primaryKey = 'id';
    protected $table = 'products';
    protected $fillable = array('name', 'description', 'price');
}
