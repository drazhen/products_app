@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="list-group">
              <a href="/product/index" class="list-group-item">List all products</a>
              <a href="/product/create" class="list-group-item">Create product</a>
            </div>
        </div>
    </div>
</div>
@endsection
