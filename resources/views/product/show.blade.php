@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-lg-8">
    <div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading">{{ $product->name }}</div>
      <div class="panel-body">
        <p>{{ $product->description }}</p>
      </div>

      <!-- List group -->
      <ul class="list-group">
        <li class="list-group-item">ID: {{  $product->id }}</li>
        <li class="list-group-item">Price: {{  $product->price }}</li>
      </ul>
    </div>
   </div>
   <div class="col-md-3">
        <div class="list-group">
          <a href="/product/show/{{ $product->id }}" class="list-group-item active">
            Show product
          </a>
          <a href="/product/index" class="list-group-item">List all products</a>
          <a href="/product/create" class="list-group-item">Create product</a>
          <a href="/product/edit/{{  $product->id }}" class="list-group-item">Edit this product</a>
        </div>
    </div>
</div>
@endsection
