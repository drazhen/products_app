@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-lg-8">
    <div class="panel panel-primary">
      <!-- Default panel contents -->
      <div class="panel-heading">Products list</div>

          <!-- Table -->
          <table class="table">
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Description</td>
                <td>Price</td>
                <td>Actions</td>
            </tr>
            @foreach($products as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ substr($value->description, 0, 80) }}</td>
                    <td>{{ $value->price }}</td>
                    <td>
                        <a class="btn btn-small btn-success" href="/product/{{ $value->id }}">Show</a>
                        <a class="btn btn-small btn-info" href="/product/edit/{{ $value->id }}">Edit</a>
                        <a class="btn btn-small btn-warning" href="/product/destroy/{{ $value->id }}">Delete</a>
                    </td>
                </tr>
            @endforeach
          </table>
      </div>
  </div>
  <div class="col-md-2">
        <div class="list-group">
          <a href="/product/index" class="list-group-item active">
            List all products
          </a>
          <a href="/product/create" class="list-group-item">Create product</a>
        </div>
    </div>
</div>
@endsection
